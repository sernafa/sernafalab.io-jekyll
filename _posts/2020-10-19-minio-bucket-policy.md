---
title:      "Minio, gestión de los Bucket Policy"
date:       2020-10-19 20:00:00
excerpt:    "Creación de Buckets Policies y asignación a usuarios"
tags:       S3 storage
toc:        false
header:
---

Minio.io es un sistema que implementa el servicio de almacenamiento basado en objetos, compatible con AWS S3.  Las siguientes notas detallan como crear un usuario, asignandole permisos en un *Bucket* concreto, es decir, crear su propio *Bucket* privado.

Primero creamos un *Bucket* nuevo sobre la instancia de Minio *qnap*

```shell
$ mcli mb qnap/sernafa-bucket
```

Generamos un usuario con sus credenciales

```shell
$ mcli admin user add qnap sernafa top_secret
```

Creamos una politica de acceso que le permite realizar todas las operaciones sobre el *Bucket*, creamos sernafa-bucket-policy.json

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
            "s3:ListBucket",
            "s3:PutObject",
            "s3:GetObject",
            "s3:DeleteObject"
        ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::sernafa-bucket/*", "arn:aws:s3:::sernafa-bucket"
      ],
      "Sid": "BucketAccessForUser"
    }
  ]
}
```

Añadimos la politica de acceso a la instancia de Minio

```bash
$ mcli admin policy add qnap sernafa-bucket sernafa-bucket-policy.json
```

Y ahora asociamos la politica con el usuario

```shell
$ mcli admin policy set qnap sernafa-bucket user=sernafa
```

Esto nos deja configurado el acceso privado para el usuario.  Se puede comprobar esta información de la instancia *Minio*

```shell
mcli admin user info qnap sernafa

AccessKey: sernafa-user
Status: enabled
PolicyName: sernafa-bucket
MemberOf:
It’s easy enough to also give multiple people access using similar policies and to also create read only policies so that everyone can see all the latest baby pictures but not add or delete them.
```
